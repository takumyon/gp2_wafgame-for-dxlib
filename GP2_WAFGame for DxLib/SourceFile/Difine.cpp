﻿#include "Difine.h"

/*-------------------------------------------*/
/* ゲーム全体の定数を管理
/*-------------------------------------------*/
const float  WINDOW_WIDTH  = 1920;			// ウィンドウの幅
const float  WINDOW_HEIGHT = 1080;			// ウィンドウの高さ

const bool   IS_WINDOWMODE = true;			// ウィンドウモード

const VECTOR ZERO_VECTOR   = VGet(0,0,0);	// ゼロベクトル