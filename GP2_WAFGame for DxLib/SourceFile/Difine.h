﻿#pragma once

#include <DxLib.h>

/*-------------------------------------------*/
/* 全体の定数を管理
/*-------------------------------------------*/
extern const float  WINDOW_WIDTH;	// ウィンドウの幅
extern const float  WINDOW_HEIGHT;	// ウィンドウの高さ

extern const bool   IS_WINDOWMODE;	// ウィンドウモード

extern const VECTOR ZERO_VECTOR;	// ゼロベクトル

/*-------------------------------------------*/
/* インライン関数群
/*-------------------------------------------*/
// インスタンスをdeleteしてNULLを入れる
template <typename T>
inline void SafeDelete(T*& p)   // ポインタの参照渡し
{
    delete (p);
    (p) = NULL;
}

// インスタンス配列をdeleteしてNULLを入れる
template <typename T>
inline void SafeDeleteArray(T*& p)
{
    delete[](p);
    (p) = NULL;
}