#pragma once

#include <DxLib.h>

/*-------------------------------------------*/
/* ゲームオブジェクトクラス
/* (すべてのオブジェクトはこれを継承する)
/*-------------------------------------------*/
class GameObject
{
public:
			 GameObject(int sourceObjectHandle,bool isDuplicateModel);	// コンストラクタ
	virtual ~GameObject();												// デストラクタ

	virtual void Initialize() = 0;
	virtual void Update()     = 0;
	virtual void Draw();

	// ゲッター・セッター関数群
	// 座標
	const VECTOR& GetPosition () const			    { return position;  }
	      void    SetPosition (  const VECTOR _set) { position = _set;  }
	// 方向
	const VECTOR& GetDirection() const			    { return direction; }
	      void    SetDirection(  const VECTOR _set) { direction = _set; }
	// 回転角			    									  
	const VECTOR& GetRotation () const			    { return rotation;  }
	      void    SetRotation (  const VECTOR _set) { rotation = _set;  }
	// 大きさ			     									  	    
	const VECTOR& GetScale    () const			    { return scale;     }
	      void    SetScale    (  const VECTOR _set) { scale = _set;     }
	// 描画中かどうか		     									  	    
	const bool    GetIsDraw   () const			    { return isDraw;    }
	      void    SetIsDraw   (  const bool   _set) { isDraw = _set;    }
	// 機能しているかどうか	   										    
	const bool    GetIsActive () const			    { return isActive;  }
	      void    SetIsActive (  const bool   _set) { isActive = _set;  }

private:
	int    objectHandle;	// オブジェクトのハンドル
	VECTOR position;		// 座標
	VECTOR direction;		// 方向
	VECTOR rotation;		// 回転角
	VECTOR scale;			// 大きさ
	bool   isDraw;			// 描画中かどうか
	bool   isActive;		// 機能しているかどうか
};