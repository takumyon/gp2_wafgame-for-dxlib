﻿#pragma once

/*-------------------------------------------*/
/* シーン / 基底クラス
/*-------------------------------------------*/
class SceneBase
{
public:
				 SceneBase();				// デストラクタ
	virtual		~SceneBase();				// コンストラクタ

	virtual void CreateGameObject()  = 0;	// オブジェクトの生成
	virtual void DestroyGameObject() = 0;	// オブジェクトの削除

	virtual void Initialize()		 = 0;	// 初期化
	virtual void Update()			 = 0;	// 更新
	virtual void Draw()				 = 0;	// 描画

private:
	// なし
protected:
	// なし
};