﻿#pragma once
#include "SceneBase.h"

/*-------------------------------------------*/
/* シーン：メインシーン / 派生クラス
/*-------------------------------------------*/
class SceneMain final : public SceneBase
{
public:
			 SceneMain();						// コンストラクタ
	virtual ~SceneMain();						// デストラクタ

	void	 CreateGameObject()  override;		// オブジェクトの生成
	void	 DestroyGameObject() override;		// オブジェクトの削除

	void     Initialize()		 override;		// 初期化
	void     Update()			 override;		// 更新
	void     Draw()				 override;		// 描画

private:
	// なし
protected:
	// なし
};