﻿#include "SceneManager.h"
#include "SceneMain.h"
#include "Difine.h"

/*-------------------------------------------*/
/* 静的インスタンスの生成
/*-------------------------------------------*/
SceneManager& SceneManager::SceneManagerInstance()
{
	static SceneManager sceneManager;
	return sceneManager;
}

/*-------------------------------------------*/
/* コンストラクタ
/*-------------------------------------------*/
SceneManager::SceneManager()
{
	// 最初のシーンを生成
	currentScene = new SceneMain();
	// シーンのオブジェクトを生成
	currentScene->CreateGameObject();
	// シーンのオブジェクトを初期化
	currentScene->Initialize();
}

/*-------------------------------------------*/
/* デストラクタ
/*-------------------------------------------*/
SceneManager::~SceneManager()
{
	// シーンのメモリを解放
	SafeDelete(currentScene);
}

/*-------------------------------------------*/
/* 更新
/*-------------------------------------------*/
void SceneManager::Update()
{
	if (currentScene != NULL)
	{
		currentScene->Update();
	}
}

/*-------------------------------------------*/
/* 描画
/*-------------------------------------------*/
void SceneManager::Draw()
{
	if (currentScene != NULL)
	{
		currentScene->Draw();
	}
}

/*-------------------------------------------*/
/* シーンの変更
/*-------------------------------------------*/
void SceneManager::ChangeScene(SceneBase *scene)
{
	if (currentScene != NULL)
	{
		// 現在のシーンの終了処理を行う
		currentScene->DestroyGameObject();	// シーンのオブジェクトを削除
		SafeDelete(currentScene);			// 現在のシーンを開放
	}

	// 現在のソーンを引数から渡されたシーンに変更する
	currentScene = scene;

	if (currentScene != NULL)
	{
		// 変更されたシーンのオブジェクトを生成
		currentScene->CreateGameObject();
		// 変更されたシーンの初期化
		currentScene->Initialize();
	}
}