﻿#pragma once

class SceneBase;

/*-------------------------------------------*/
/* シーン / 管理クラス
/*-------------------------------------------*/
class SceneManager final
{
public:

	// 静的インスタンスを生成
	static SceneManager& SceneManagerInstance();

		~SceneManager();					// デストラクタ

	void Update();							// 更新
	void Draw();							// 描画

	void ChangeScene(SceneBase* scene);		// シーンの変更

private:
					SceneManager();			// シングルトン
		 SceneBase *currentScene;			// 現在のシーン

protected:
	// なし
};

#define SCENE_MANAGER SceneManager::SceneManagerInstance()