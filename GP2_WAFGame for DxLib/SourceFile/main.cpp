﻿#include <DxLib.h>
#include "SceneManager.h"
#include "Difine.h"

int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	// DXライブラリの初期化処理
	if (DxLib_Init() == -1)
	{
		// エラーがおきたら終了
		MessageBox(GetMainWindowHandle(), "DXライブラリの初期化に失敗しました。", NULL, MB_OK);
		return -1;
	}

	// 画面モードのセット
	SetGraphMode(WINDOW_WIDTH, WINDOW_HEIGHT, 16);
	// ウィンドウモード
	ChangeWindowMode(IS_WINDOWMODE);

	while (ProcessMessage() == 0 && CheckHitKey(KEY_INPUT_ESCAPE) == false)
	{
		// 更新処理
		SCENE_MANAGER.Update();

		// 画面のクリア
		ClearDrawScreen();

		// 描画処理
		SCENE_MANAGER.Draw();

		// 裏画面の内容を反映
		ScreenFlip();
	}

	// DXライブラリの終了処理
	DxLib_End();

	return 0;
}